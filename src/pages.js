import VueRouter from 'vue-router'
import Item from './components/Item.vue'

export default new VueRouter({
    routes:[
        {
            path: '',
            component: Item
        },
        {
            path:'/p/:id',
            component: Item
        }
    ],
    mode: 'history',
    scrollBehavior(){
        return {
            x:0,
            y:0,
        }
    }
})